<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fishpig_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1gj#RVt>Xr @6~:Ik,])qNTJI$$n$j:?[%?lcfg?.Eo#j1,ezkcA-tw1rzN]$T`w' );
define( 'SECURE_AUTH_KEY',  'MQ#(bk6XQnLe73Njizo=:K*IBU:?wUSx5?Idk{hfcVs9GV},r2&Z;.]frUC35VDO' );
define( 'LOGGED_IN_KEY',    'xD;#Q;ji/zDA^.|mm;{N)mM*]}hDr)}E!1eW2h/(!q$b6i2 E2DF#<Sn3tZ3&xcT' );
define( 'NONCE_KEY',        'yD{F(F8nK8PWP/-eC6eluuXi--{(/&^>jJK>PGXtw z#5jCPf)2~IIO)6$3sFw8&' );
define( 'AUTH_SALT',        'r4!dw,EG,Yij{V%w,#x9,i5#c4eu}FU5r39:V-b|^K/Z3IeIv>:l^V0 +v)F>W=$' );
define( 'SECURE_AUTH_SALT', 'bV.Lk D@67RQK$M2L</kx.6=E#^[7oO~k~EFGXnXzN2>E.^L:*b%TyxJa-MG3|-I' );
define( 'LOGGED_IN_SALT',   'O2D<q-(7W*%Z_f%C*ac,_zf3+E9fns(]^`w[FbMfAncjp%{@*Y/5][`6[7[JzEKe' );
define( 'NONCE_SALT',       'vsFvpw;1<w2.9=zGpuCpda-zV$C?QMKf99:+J&E*^k!Yg)0`kE#QVs-]~Ngs:d9t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
